import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created: 05.03.2023
 *
 * @author Barbos Ioana-Denisa (barbo)
 */
public class DeleteLines {

    public static void main(String[] args) {

        String input = "";
        String output = "";
        ArrayList<String> row = new ArrayList<String>();

        if (args.length >= 3) {
            input = args[0];
            output = args[1];
            for (int i = 2; i < args.length; i++) {
                row.add(args[i]);
            }
        }

        HashSet<Integer> linesDel = new HashSet<>();

        for (String numbers : row) {
            String[] s = numbers.split("-");
            int start = Integer.parseInt(s[0]);
            int end = (s.length == 1) ? start : Integer.parseInt(s[1]);
            for (int i = start; i <= end; i++) {
                linesDel.add(i);
            }
        }

        try (FileOutputStream fileOutputStream = new FileOutputStream(output)){

            File f = new File(input);
            AsciiInputStream asciiInputStream = new AsciiInputStream(f);

            int number = 1;
            String line;
            while ((line = asciiInputStream.readLine())!= null){
                if (!linesDel.contains(number)){
                    fileOutputStream.write(line.getBytes());
                    fileOutputStream.write('\n');
                }
                number++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
class AsciiInputStream extends FileInputStream{

    public AsciiInputStream(File file) throws FileNotFoundException {
        super(file);
    }


    public String readLine() throws IOException{
        StringBuffer s = new StringBuffer();
        int c;
        while ((c = this.read())!= -1){ //end of stream
            if (c == '\n'){
                break;
            }
            s.append((char) c);
        }
        if (c == -1 && s.length() == 0){
            return null;
        }

        return s.toString();
    }
}