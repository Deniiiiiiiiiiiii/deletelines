import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * Created: 05.03.2023
 *
 * @author Barbos Ioana-Denisa (barbo)
 */
public class KommunikationKette {

    public static void main(String[] args) throws IOException {
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        PipedInputStream pipedInputStream = new PipedInputStream(pipedOutputStream);

        StreamCopier.copyByte(System.in, pipedOutputStream);
        pipedOutputStream.close();

        FileOutputStream f = new FileOutputStream("output.txt");
        StreamCopier.copyBuffered(pipedInputStream, f, 4);
        f.close();
    }

}
